'user strict'

let express = require('express');
let cors = require('cors');
let bodyparser = require('body-parser');

let app = express();

//IMPORT ROUTES
let songRouter = require('./routes/songRouter');
let userRouter = require('./routes/userRouter');

//CORS
app.use(cors());

//BODY PARSER
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended: true }));

//ROUTES
app.use('/api', songRouter);
app.use('/api', userRouter);

module.exports = app;