'use strict'

let { Op } = require('sequelize');
let songModel = require('../models/song');

async function getAll(request, response) {
    let offset = request.params.offset || 0;
    let limit = request.params.limit || -1;

    if(limit < 1) {
        let rowsFounded = await songModel.findAll({
            where: {
                visible: {
                    [Op.eq]: true
                }
            },
            offset: offset
        });
    
        response.status(200).send(rowsFounded);
    } else {
        let rowsFounded = await songModel.findAll({
            where: {
                visible: {
                    [Op.eq]: true
                }
            },
            offset: offset,
            limit: limit
        });
    
        response.status(200).send(rowsFounded);
    }
}

async function getSongByID(request, response) {
    let songId = request.params.id;

    let rowsFounded = await songModel.findAll({
        where: {
            [Op.and]: [
                { id: songId },
                { visible: true }
            ]
        }
    });

    if(rowsFounded.length > 0) {
        response.status(200).send(rowsFounded[0]);
    } else {
        response.status(409).send({ message: 'Song not found' });
    }
}

module.exports = {
    getAll,
    getSongByID
}