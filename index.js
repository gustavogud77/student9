'user strict'

let db = require('./database/db');
let app = require('./app');

let port = 3000;

db.authenticate()
.then(async () => {
    await db.sync({ alter: true });

    app.listen(port, () => {
        console.log('Server up.');
    });
});