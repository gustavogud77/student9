'use strict'

let express = require('express');
let api = express.Router();
let userController = require('../controllers/userController');
let auth = require('../middlewares/auth');

api.get('/users/favorites', auth.auth, (request, response) => {
    userController.getFavoritesByUser(request, response);
});
api.post('/users/addFavorite', auth.auth, (request, response) => {
    userController.addFavorite(request, response);
});
api.post('/users/register', (request, response) => {
    userController.register(request, response);
});
api.post('/users/login', (request, response) => {
    userController.login(request, response);
});

module.exports = api;