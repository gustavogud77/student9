'use strict'

let db = require('../database/db');
let { DataTypes, Deferrable } = require('sequelize');
let UserModel = require('./user');
let SongModel = require('./song');

const Favorites = db.define('Favorites', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    user_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
            model: UserModel,
            key: 'id',
            deferrable: Deferrable.INITIALLY_IMMEDIATE
        }
    },
    song_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
            model: SongModel,
            key: 'id',
            deferrable: Deferrable.INITIALLY_IMMEDIATE
        }
    },
    visible: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: true
    }
});

module.exports = Favorites;