'use strict'

let db = require('../database/db');
let { DataTypes } = require('sequelize');

const Song = db.define('Song', {
    id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    artista: {
        type: DataTypes.STRING(50),
        allowNull: false
    },
    name: {
        type: DataTypes.STRING(100),
        allowNull: false,
    },
    album: {
        type: DataTypes.STRING(50),
        allowNull: false
    },
    released: {
        type: DataTypes.DATEONLY,
        allowNull: false,
        defaultValue: DataTypes.NOW()
    },
    playtime: {
        type: DataTypes.STRING(10),
        allowNull: false,
        defaultValue: '00:00'
    },
    tracknumber: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 1
    },
    visible: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: true
    }
});

module.exports = Song;