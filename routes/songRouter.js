'use strict'

let express = require('express');
let api = express.Router();

let songController = require('../controllers/songController');

api.get('/songs/all/:offset/:limit',(request, response) => {
    songController.getAll(request, response);
});
api.get('/songs/:id', songController.getSongByID);

module.exports = api;