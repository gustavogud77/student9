'use strict'

let { Op } = require('sequelize');
let favoritesModel = require('../models/favorites');
let songsModel = require('../models/song');
let userModel = require('../models/user');
let bcrypt = require('bcrypt');
let jwt = require('../services/jwt');

async function getFavoritesByUser(request, response) {
    let userId = request.body.id;
    let title = request.body.title;
    let author = request.body.author;

    let rowsFounded = await favoritesModel.findAll({
        where: {
            [Op.and]: [
                {
                    user_id: {
                        [Op.eq]: userId
                    }
                },
                { visible: true }
            ]
        }
    });

    if(rowsFounded.length > 0) {
        let songsFounded = [];
        rowsFounded.forEach(async (favorite) => {
            let songFounded = await songsModel.findAll({
                where: {
                    id: favorite.song_id
                }
            });
            songsFounded.push(songFounded[0]);
        });
        setTimeout(() => {
            let songsMatch = songsFounded.filter((song) => {
                let authorRegex = new RegExp(`${author}+`,'g');
                let titleRegex = new RegExp(`${title}+`,'g');
                let itMatches = (authorRegex.test(song.artista) || titleRegex.test(song.name))
                                ? true
                                : false;
                return itMatches;
            });

            response.status(200).send(songsMatch);
        },1000);
    } else {
        response.status(409).send({ message: 'Data not found' });
    }
}
async function addFavorite(request, response) {
    let createdRow = await favoritesModel.create(request.body);

    response.status(201).send(createdRow);
}
async function register(request, response) {
    let userData = request.body;

    let existingRows = await userModel.findAll({
        where: {
            [Op.and]: [
                { username: userData.username },
                { visible: true }
            ]
        }
    });

    if(existingRows.length > 0) {
        response.status(409).send({ message: 'User already exists' });
    } else {
        bcrypt.genSalt(3, function(err, salt) {
            bcrypt.hash(userData.password, salt, async function(err, hash) {
                if(hash) {
                    userData.password = hash;
                    let createdRow = await userModel.create(userData);
                    response.status(201).send(createdRow);
                }
            });
        });
    }
}
async function login(request, response) {
    let userData = request.body;

    let rowsFounded = await userModel.findAll({
        where: {
            [Op.and]: [
                { username: userData.username },
                { visible: true }
            ]
        }
    });

    if(rowsFounded.length > 0) {
        bcrypt.compare(userData.password, rowsFounded[0].password,function(err, res) {
            if(res) {
                let token_data = jwt.getToken(rowsFounded[0]);
                response.status(200).send(token_data);
            }
        });
    }
}

module.exports = {
    getFavoritesByUser,
    addFavorite,
    register,
    login
}