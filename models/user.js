'use strict'

let { DataTypes } = require('sequelize');
let db = require('../database/db');

const User = db.define('User', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    username: {
        type: DataTypes.STRING(10),
        allowNull: false,
    },
    password: {
        type: DataTypes.STRING(300),
        allowNull: false
    },
    visible: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: true
    }
});

module.exports = User;