'use strict'

let jwt = require('jwt-simple');
let moment = require('moment');
let secret = 'abc123';

exports.auth = (request, response, next) => {
    if(!request.headers.authorization) {
        return response.status(409).send({ message: 'No authorization' });
    } else {
        let token = request.headers.authorization.replace(/['"]+/g,'');
        let data = jwt.decode(token, secret);

        if(data.exp_at <= moment().unix()) {
            return response.status(400).send({ message: 'token expired' });
        } else {
            request.usuario = data;
        }
    }
    next();
}