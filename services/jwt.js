'use strict'

let jwt = require('jwt-simple');
let moment = require('moment');
let secret = 'abc123';

exports.getToken = (userData) => {
    let exp_at = moment().add(30,'days');
    userData.exp_at = exp_at;
    let token = jwt.encode(userData, secret);
    let token_data = {token, exp_at};
    
    return token_data;
}